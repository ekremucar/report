package org.orcateam.report;


/**
 * OrcaReport Context Param Key Definations
 */
public class OrcaReportParam {

    public static String NULL_STRING = "org.orcateam.report.nullString";

    public static String RESOURCE_BUNDLE = "org.orcateam.report.resourceBundle";

    public static String LOCALE = "org.orcateam.report.locale";

}
