package org.orcateam.report.expression;

import org.apache.commons.lang3.StringUtils;
import org.orcateam.report.util.ELUtil;

import java.util.Map;

public class ExpressionVal {

    private String key;
    private String expression;
    private Object value;

    public ExpressionVal() {

    }

    public ExpressionVal(String key, Map<String, Object> context) {
        this.key = key;
        this.expression = trimTheEL(key);
        invoke(context);
    }

    private String trimTheEL(String value) {
        return StringUtils.remove(StringUtils.remove(value, "${"), "}");
    }

    public Object invoke(Map<String, Object> context) {
        if (expression != null) {
            value = ELUtil.invoke(expression, context);
        }
        return value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
