package org.orcateam.report.exception;

public class OrcaReportRendererException extends Exception {

    public OrcaReportRendererException() {
    }

    public OrcaReportRendererException(String s) {
        super(s);
    }

    public OrcaReportRendererException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public OrcaReportRendererException(Throwable throwable) {
        super(throwable);
    }
}
