package org.orcateam.report.engine.docx.renderer;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.orcateam.report.OrcaReport;
import org.orcateam.report.engine.docx.DocXEngine;
import org.orcateam.report.exception.OrcaReportRendererException;
import org.orcateam.report.expression.ExpressionVal;
import org.orcateam.report.util.TokenUtil;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Orca Report Docx Paragraph Renderer Class.
 */
public class DocXParagraphRenderer extends DocXRenderer {

    List<String> tokenList = null;
    Map<String, Object> reportContext = new HashMap<String, Object>();

    public DocXParagraphRenderer(Object paragraph, OrcaReport orcaReport) {
        super(paragraph, orcaReport);
    }

    public DocXParagraphRenderer(Object paragraph, OrcaReport orcaReport, Map<String, Object> context) {
        super(paragraph, orcaReport);
        if (context != null) {
            for (String key : context.keySet()) {
                putContext(key, context.get(key));
            }
        }
    }

    /**
     * Render The Docx Paragraph
     *
     * @throws OrcaReportRendererException
     */
    @Override
    public void render() throws OrcaReportRendererException {
        init();
        Collection<ExpressionVal> expressionValList = getExpressionValues();
        if (expressionValList != null && !expressionValList.isEmpty()) {
            for (ExpressionVal expressionVal : expressionValList) {
                boolean notFinished = true;
                try {
                    do {
                        String prevHash = DigestUtils.md5Hex(getParagraph().getParagraphText());
                        DocXEngine.replaceParagraphText(expressionVal.getKey(), expressionVal.getValue(), getParagraph(), getOrcaReport());
                        String newHash = DigestUtils.md5Hex(getParagraph().getParagraphText());
                        String paragText = getParagraph().getParagraphText();
                        if (newHash.equals(prevHash)) {
                            throw new OrcaReportRendererException("Error while replacing : \"" + expressionVal.getKey() + "\". " +
                                    "Either you have given an expression that should be replaced with itself or something got fuxored. " +
                                    "If so, please report this issue to the Orcateam.");
                        } else if (!StringUtils.contains(paragText, expressionVal.getKey())) {
                            notFinished = false;
                        }
                    } while (notFinished);
                } catch (OrcaReportRendererException ex) {
                    // logger warning
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Initialize available paragraph tokens and invoke expression
     */
    @Override
    protected void init() {
        initTokenList();
        initReportContext();
        initExpressionAndInvoke();
    }

    /**
     * Read token definition and init tokenList.
     */
    private void initTokenList() {
        String template = getParagraph().getParagraphText();
        tokenList = TokenUtil.getTokenList(template);
    }

    /**
     * Read orcaReport and initialize context
     */
    private void initReportContext() {
        for (String key : getOrcaReport().getContextKeySet()) {
            //TODO burada bakılacak replaced text olup olmadığı ve neler yapılacağına karar verilecek.
            reportContext.put(key, getOrcaReport().get(key));
        }
    }

    public void putContext(String key, Object value) {
        reportContext.put(key, value);
    }

    /**
     * Invoke expression and putExpression to renderer context.
     */
    private void initExpressionAndInvoke() {
        if (tokenList != null) {
            for (String token : tokenList) {
                //TODO burada el çevirilirken exceptionlar fırlatabiliyor. kullanıcıyı uyaracak exceptionlar alabiliriz.
                //Ambiguous statement, missing ';' between expressions gibi
                putExpression(token, new ExpressionVal(token, reportContext));
            }
        }
    }

    public XWPFParagraph getParagraph() {
        return (XWPFParagraph) getBody();
    }

}
