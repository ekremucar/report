package org.orcateam.report.util;


import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TokenUtil {

    public static String getToken(String template) {
        return getToken(template, "\\$\\{(.+?)\\}");
    }

    public static String getToken(String template, String regex) {
        List<String> tokenList = getTokenList(template, regex);
        return (tokenList != null && !tokenList.isEmpty()) ? tokenList.get(0) : null;
    }

    public static List<String> getTokenList(String template) {
        return getTokenList(template, "\\$\\{(.+?)\\}");
    }

    public static List<String> getTokenList(String template, String regex) {
        List<String> tokenList = null;
        if (!StringUtils.isEmpty(template)) {
            tokenList = new ArrayList<String>();
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(template);
            while (matcher.find()) {
                tokenList.add(trim(matcher.group()));
            }
        }
        return tokenList;
    }

    public static String trim(String expression) {
        return ELUtil.apostropheCorrection(StringUtils.trim(expression));
    }

}
