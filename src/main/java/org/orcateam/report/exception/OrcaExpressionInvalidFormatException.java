package org.orcateam.report.exception;

public class OrcaExpressionInvalidFormatException extends Exception {

    public OrcaExpressionInvalidFormatException() {
    }

    public OrcaExpressionInvalidFormatException(String s) {
        super(s);
    }

    public OrcaExpressionInvalidFormatException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public OrcaExpressionInvalidFormatException(Throwable throwable) {
        super(throwable);
    }
}
