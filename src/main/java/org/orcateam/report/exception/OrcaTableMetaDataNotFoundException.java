package org.orcateam.report.exception;

public class OrcaTableMetaDataNotFoundException extends Exception {


    public OrcaTableMetaDataNotFoundException() {

    }

    public OrcaTableMetaDataNotFoundException(String s) {
        super(s);
    }

    public OrcaTableMetaDataNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public OrcaTableMetaDataNotFoundException(Throwable throwable) {
        super(throwable);
    }
}
