package org.orcateam.report.engine.docx.renderer;

import org.orcateam.report.OrcaReport;
import org.orcateam.report.OrcaReportRenderer;

public abstract class DocXRenderer extends OrcaReportRenderer {

    private Object body;

    protected abstract void init() throws Exception;

    public DocXRenderer(Object body, OrcaReport orcaReport) {
        super(orcaReport);
        this.body = body;
    }

    public Object getBody() {
        return body;
    }

}
