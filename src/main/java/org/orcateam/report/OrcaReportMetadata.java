package org.orcateam.report;

public class OrcaReportMetadata {

    private String key;
    private String var;
    private String value;

    public OrcaReportMetadata() {
    }

    public OrcaReportMetadata(String key, String var, String value) {
        this.key = key;
        this.var = var;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
